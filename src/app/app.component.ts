import { Component, OnInit } from '@angular/core';
import { interval } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent implements OnInit {
  ngOnInit() {
     this.generateWords();
  }
  
  generateWords() { 

    const intervalValue = interval(3000);
    intervalValue.subscribe(() => {
      let possibleSymbols = '0123456789qwertyuiopasdfghjklzxcvbnmQWERTYUIOPASDFGHJKLZXCVBNM';
      let text = this.generateText(possibleSymbols);

      if(this.isIncludeZero(text)) { 
        console.log(text + '- 0 exists');
        return this.setStyleAndContentDocument("", "");
      }
      
      if(this.isPalindrome(text)) {
        console.log(text +'- palindrome');
        return this.setStyleAndContentDocument("red", text);
      }

      if(this.isNumber(text)) {
        console.log(text + '- numbers');
        return this.setStyleAndContentDocument("blue", text);
      }
      this.setStyleAndContentDocument("black", text);
    });
  }

  generateText(possibleSymbols: string){
    let text = '';
    for(let i = 0; i < 5; i++) {
      text += possibleSymbols.charAt(Math.floor(Math.random() * possibleSymbols.length));
    }
    return text;
  }

  setStyleAndContentDocument(color: string, text: string){
    document.getElementById("possibleSymbols").style.color = color;
        return document.querySelector('#possibleSymbols').textContent = text;
  }

  isPalindrome(str: string) { 
    let strReverse = str.split('').reverse().join('');
    if(strReverse == str) { 
      return true;
    } else { 
      return false;
    }
  }

  isNumber(str: string) {
    return /^-?[\d.]+(?:e-?\d+)?$/.test(str);
  }

  isIncludeZero(str: string) { 
    return str.includes('0');
  }
};